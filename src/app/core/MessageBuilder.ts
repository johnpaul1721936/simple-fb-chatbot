import { Service } from "./interfaces/service.interface";

class MessageBuilder {

    private dialog: any = {};

    public intent(name: string, callback: any) {
        this.dialog[name] = callback;
    }

    async buildConversation(intent: string, service: Service) {
        try {
            await this.dialog[intent](service);
        } catch (error) {
            await this.dialog['FALLBACK'](service);
        }
    }
}

export default new MessageBuilder();