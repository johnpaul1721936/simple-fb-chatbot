import { getMenus } from "@/app/model/menu.model";
import messageBuilder from "../../MessageBuilder"
import { Facebook } from "../Facebook"
import { genericTemplate, quickReply, textMessage } from "../../services/mesage.service";
import { AttachmentElement } from "../interfaces/message.interface";

messageBuilder.intent('GET_STARTED', async (facebook: Facebook) => {
    const menus = await getMenus();
    console.log(menus);
    const attachmentElements = menus?.map(item => {
        return {
            "title": item.menu,
            "image_url": 'https://raw.githubusercontent.com/fbsamples/original-coast-clothing/main/public/styles/male-work.jpg',
            "subtitle": 'Test Subtitle',
            "buttons": [
                {
                    "type": "postback",
                    "title": "Check Order Status",
                    "payload": item.intent
                }
            ]
        }
    }) as AttachmentElement[];
    facebook.sendReply(genericTemplate(attachmentElements))
});

messageBuilder.intent('FALLBACK', async (facebook: Facebook) => {
    return await facebook.sendReply(textMessage('Sorry, I did not understand that'));
});

