import messageBuilder from "../../MessageBuilder"
import { Facebook } from "../Facebook"
import { genericTemplate, quickReply, textMessage } from "../../services/mesage.service";
import { AttachmentElement } from "../interfaces/message.interface";
import { findIntent, getIntents } from "@/app/model/intent.model";
import { getLastMessage, storeMessage } from "@/app/model/message.model";

//All the intents from the records stored in the database
const fetchIntents = async() => {
    const intents = await getIntents();
    intents?.map(item => {
        messageBuilder.intent(item.intent, async (facebook: Facebook) => {

            if (item.intent === 'END_INTENT') {
                return await facebook.sendReply(quickReply("Well done. If you have more inquires you can start over and select a topic from the menus",[
                    {
                        "content_type": "text",
                        "title": "Start Over",
                        "payload": "GET_STARTED"
                    },
                    {
                        "content_type": "text",
                        "title": "Exit",
                        "payload": "END_INTENT"
                    }
                ]));
            }

            return await facebook.sendReply(textMessage(item.message));
        });
    });
}

fetchIntents();