export interface Message {
    attachment?: Attachment;
    message?: {
        text: string;
    } 
}

export interface Attachment {
    type: string;
    payload: {
        template_type: string;
        elements: AttachmentElement[];
    }
}

export interface AttachmentElement {
    title: string;
    image_url: string;
    subtitle: string;
    default_action?: Button;
    buttons: Button[];
}

export interface Button {
    type: string;
    url?: string;
    title?: string;
    webview_height_ratio?: string;
    payload?: string;
}

export interface QuickReply {
    content_type: string;
    title: string;
    payload: string;
    image_url?: string;
}