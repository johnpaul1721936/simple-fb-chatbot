export interface Event {
    sender: {
        id: string
    },
    recipient: {
        id: string
    },
    timestamp: number,
    postback?: {
        title: string,
        payload: string
    },
    message?: {
        mid?: string,
        text: string,
        nlp?: Object,
        quick_reply?: {
            payload: string
        }
    }
}