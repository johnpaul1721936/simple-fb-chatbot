import { Attachment, QuickReply } from "../interfaces/message.interface";

export const MessageType = {
    TEXT: 'text',
    TEMPLATE: 'template'
}

export const MessageTemplateType = {
    GENERIC: 'generic'
}

export interface Message {
    attachment?: Attachment;
    text?: string;
    quick_replies?: QuickReply[];
}