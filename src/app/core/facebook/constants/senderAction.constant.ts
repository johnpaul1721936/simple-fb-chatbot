export const SenderAction = {
    TYPING_ON: 'typing_on',
    TYPING_OFF: 'typing_off',
    MARK_SEEN: 'mark_seen',
}