import { httpPost } from "@/app/core/services/http.service";
import { SenderAction } from "./constants/senderAction.constant";
import { Event } from "./interfaces/event.interface";
import { Service } from "../interfaces/service.interface";
export class Facebook implements Service {

    private event: Event;
    private facebookUrl: string = '';

    constructor(event: Event) {
        this.event = event;
        try {
            this.facebookUrl = `${process.env.FB_GRAPH_URL}?access_token=${process.env.PAGE_TOKEN}`;
        } catch (error) {
            
        }
    }

    getSenderId() {
        return this.event.sender.id;
    }

    getRecipientId() {
        return this.event.recipient.id;
    }

    getPostBackTitle() {
        return this.event.postback?.title;
    }

    getPostBackPayload() {
        return this.event.postback?.payload;
    }

    async sendReply(message: string|Object) {

        try {
            const payload = {
                recipient: {
                    id: this.getSenderId(),
                },
                message
            };
            this.triggerTyping();
            await httpPost(this.facebookUrl, payload);
            this.triggerTyping(SenderAction.TYPING_OFF);
        } catch (error: any) {
            console.log(error.response.data);
        }
    }

    async markSeen() {
        const payload = {
            recipient: {
                id: this.getSenderId(),
            },
            sender_action: SenderAction.MARK_SEEN
        }

        return await httpPost(this.facebookUrl, payload);
    }

    private async triggerTyping(status: string = SenderAction.TYPING_ON) {
        const payload = {
            recipient: {
                id: this.getSenderId(),
            },
            sender_action: status
        }

        return await httpPost(this.facebookUrl, payload);
    }
}