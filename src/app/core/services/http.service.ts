import axios from "axios";

export const httpPost = async (url: string, payload: any, options?: any) => {
    const headers = {
        'Content-Type': 'application/json'
    }
    return await axios.post(url, payload, {...options, headers: headers});
}