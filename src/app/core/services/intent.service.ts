import { NextApiRequest, NextApiResponse } from "next";
import messageBuilder from '@/app/core/MessageBuilder';
import { Event } from '@/app/core/facebook/interfaces/event.interface';
import '../facebook/messages/index';
import { Facebook } from '@/app/core/facebook/Facebook';
import { findIntent } from "@/app/model/intent.model";
import { getLastMessage, storeMessage } from "@/app/model/message.model";

export const sendMessage = async (req: NextApiRequest, res: NextApiResponse<any>) => {
    const event = req.body.entry[0].messaging[0] as Event;
    const builder = messageBuilder;
    const facebook = new Facebook(event);
    let intent = (event.postback?.payload || event?.message?.quick_reply?.payload || event?.message?.text) as string

    if (event?.message?.text) {
        const lastMessage = await getLastMessage(facebook.getSenderId());
        console.log('lastMessage', lastMessage);
        intent = lastMessage?.currentIntent || '';
    }

    await saveMessage(facebook.getSenderId(), intent);

    await builder.buildConversation(intent, facebook);
    return res.status(200).json({message: event});
}

const saveMessage = async (userId: string, intent: string) => {
    const currentIntent = await findIntent(intent);
    await storeMessage({
        currentIntent: currentIntent?.nextIntent || '',
        userId,
    });
}