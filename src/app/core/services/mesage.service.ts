import { MessageTemplateType, MessageType, Message } from "../facebook/constants/message.constant";
import { AttachmentElement, QuickReply } from "../facebook/interfaces/message.interface";

export const genericTemplate = (elements: AttachmentElement[]): Message => {
    return  {
        attachment: {
            type: MessageType.TEMPLATE,
            payload: {
                template_type: MessageTemplateType.GENERIC,
                elements
            }
        }
    }
}

export const textMessage = (text: string): Message => {
    return {text};
}

export const quickReply = (text: string, quickReplies: QuickReply[]): Message => {
    return {
        text,
        quick_replies: quickReplies
    }
}