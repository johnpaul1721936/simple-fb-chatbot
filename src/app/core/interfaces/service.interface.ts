export interface Service {
    sendReply(message: string): any;
}