import { PrismaClient, Prisma, Menu } from '@prisma/client'
const prisma = new PrismaClient()

export const getMenus = async(): Promise<Menu[] | null> => {
    const menus =  await prisma.menu.findMany();
    return menus;
}