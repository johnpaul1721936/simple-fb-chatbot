import { PrismaClient, Prisma, IntentFlow, Messages } from '@prisma/client'
const prisma = new PrismaClient()

export const storeMessage = async(mesage: {userId: string, currentIntent: string}): Promise<void | null> => {
    try {
        await prisma.messages.create({
            data: {
                userId: mesage.userId,
                currentIntent: mesage.currentIntent,
                message: 'Test Message'
            }
        });
    } catch (error: any) {
        console.log(error);
    }
}

export const getLastMessage = async(userId: string): Promise<Messages | null> => {
    const lastMessage = await prisma.messages.findFirst({
        where: {
            userId: userId
        },
        orderBy: {
            createdAt: 'desc'
        }
    });
    return lastMessage;
}