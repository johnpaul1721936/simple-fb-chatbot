import { PrismaClient, Prisma, IntentFlow } from '@prisma/client'
const prisma = new PrismaClient()

export const getIntents = async(): Promise<IntentFlow[] | null> => {
    const intents =  await prisma.intentFlow.findMany();
    return intents;
}

export const findIntent = async(intent: string): Promise<IntentFlow | null> => {
    const getIntent = await prisma.intentFlow.findFirst({
        where: {
            intent: intent
        }
    });
    return getIntent;
}