import type { NextApiRequest, NextApiResponse } from 'next';
import { sendMessage } from '@/app/core/services/intent.service';

type ResponseData = {
  message: string;
};

const handler = async(req: NextApiRequest, res: NextApiResponse<ResponseData>) => {
  if (req.method === 'GET') {
    return verifyWebhook(req, res);
  }

  if (req.method === 'POST') {
    return sendMessage(req, res);
  }
}

const verifyWebhook = (req: NextApiRequest, res: NextApiResponse<any>) => {
	// Parse the query params
	const mode = req.query['hub.mode'];
	const token = req.query['hub.verify_token'];
	const challenge = req.query['hub.challenge'];

	if (mode && token) {
    if (mode === 'subscribe' && token === process.env.VERIFY_TOKEN) {
      console.log('WEBHOOK_VERIFIED');
      return res.status(200).send(challenge);
    } else {
      return res.status(403).json({ message: 'Invalid Token' }); 
    }
  }
  return res.status(500).json({message: 'Invalid token'});
}

export default handler;